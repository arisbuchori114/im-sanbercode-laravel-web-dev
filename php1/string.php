<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Berlatih String PHP  <h1>

    <?php
        echo " <h3> Soal 1</h3>";
        $kalimat1 = "PHP is never old";
        echo  "kalimat pertama :" . $kalimat1 . "<br>";
        echo " panjang kalimat 1 :" . strlen($kalimat1) . "<br>";
        echo " jumlah kata kalimat 1:" . str_word_count($kalimat1) . "<br>";

        echo " <h3> Soal 2 </h3>";
        $Kalimat2 = "I love PHP";
        echo "kalimat kedua :" . $Kalimat2 . "<br>";
        echo "kata 1 kalimat kedua :" . substr($Kalimat2,0,1) . "<br>";
        echo "kata 2 kalimat kedua :" . substr($Kalimat2,2,5) . "<br>";
        echo "kata 3 kalimat kedua :" . substr($Kalimat2,6,8) . "<br>";

        echo " <h3> Soal 3 </h3>";
        $kalimat3 = "PHP is old but sexy ";
        echo "kalimat ketiga :" . $kalimat3 . "<br>";
        echo " ganti string kalimat ke 3 :" . str_ireplace("sexy","awesome", $kalimat3);
        




    ?>
    
</body>
</html>