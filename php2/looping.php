<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping </h1>
    <?php
     echo "<h3>Soal No 1 Looping I Love PHP</h3>";

     echo"<h6>Looping 1</h6>";
     $x = 2;
     do{
        echo $x  . "-I Love PHP <br>";
        $x +=2;
     }while($x <=20);

     echo"<h6>Looping 2</h6>";
     $y=20;
     while($y >= 2){
        echo $y . "- I Love PHP <br>";
        $y -=2;
     }

     echo "<h3>Soal No 2 Looping array modulo</h3>";

     $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);

        foreach($numbers as $value){
            $rest[] = $value%=2;
        }

        echo"<br>";
        echo"Array sisa bagian adalah : ";
        print_r($rest);

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($items as $val){
            $tampung = [
                'ID'=>$val[0],
                'Name '=>$val[1],
                'Price'=>$val[2],
                'Description'=>$val[3],
                'Source'=>$val[4],
            ];
            print_r($tampung);
            echo "<br>";
        }

        echo "<h3>Soal No 4 Asterix </h3>";

        for($j=5; $j>=0; $j--){
            for($r=5; $r>$j; $r--){
                echo" * ";

            }
        
            echo"<br>";
        }



    ?>
</body>
</html>